
****
Blob
****

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Blob`

Similar to :doc:`Draw </sculpt_paint/sculpting/tools/clay>`,
but vertices are pushed outwards like an inverted pinching effect.
This will lead to a more consistent spherical curvature and thickening of strokes.


Brush Settings
==============

General
-------

Magnify
   By default at 0.5 to push out the mesh during the stoke.
   More info at :ref:`Pinch/Magnify <bpy.types.Brush.crease_pinch_factor>`

.. note::

   More info at :ref:`sculpt-tool-settings-brush-settings-general` brush settings
   and on :ref:`sculpt-tool-settings-brush-settings-advanced` brush settings.
